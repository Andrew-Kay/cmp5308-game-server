
package bcu.cmp5308.gameserver;

import java.io.*;
import java.net.Socket;

public class GameConnection implements Runnable {
    public static boolean validUsername(String username) {
        return username.matches("[a-zA-Z]{3,20}");
    }
    public static boolean validChannelName(String channelName) {
        return channelName.matches("#[a-zA-Z0-9\\-_]{3,20}");
    }
    
    private static String commandArg(String cmd) {
        return cmd.substring(cmd.indexOf(' ') + 1);
    }
    
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    
    private GameServer server;
    private String username, channelName;
    private GameConnection opponent;
    
    public GameConnection(GameServer server, Socket socket) throws IOException {
        this.server = server;
        this.socket = socket;
        
        InputStreamReader isr = new InputStreamReader(socket.getInputStream());
        this.in = new BufferedReader(isr);

        OutputStream os = socket.getOutputStream();
        this.out = new PrintWriter(os);
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getChannelName() {
        return channelName;
    }
    
    @Override
    public String toString() {
        if(username == null) {
            return String.format("@%x", hashCode());
        } else if(channelName == null) {
            return username;
        } else {
            return username + channelName;
        }
    }
    
    void send(String msg, Object... args) {
        if(!isClosed()) {
            if(args.length > 0) {
                msg = String.format(msg, args);
            }
            
            System.out.println(this + " >> " + msg);
            out.println(msg);
            out.flush();
        }
    }
    
    String receive() throws IOException {
        if(isClosed()) {
            return null;
        } else {
            String line = in.readLine();
            if(line == null) {
                throw new IOException("Received null from connection " + this + ".");
            }
            
            System.out.println(this + " << " + line);
            return line;
        }
    }
    
    public void close() {
        server.deregisterConnection(this);
        if(!isClosed()) {
            try {
                send("GOODBYE %s", username);
                socket.close();
            } catch(IOException e2) {
                System.err.println("While closing, connection " + this + " caused " + e2);
            }
        }
    }
    
    public boolean isClosed() {
        return socket.isClosed();
    }
    
    void setOpponent(GameConnection opponent) throws GameProtocolException {
        if(this.opponent != null) {
            throw new GameProtocolException("Connection " + this + " already has opponent.");
        }
        
        this.opponent = opponent;
    }
    
    @Override
    public void run() {
        String cmd;
        try {
            System.out.println("Connection " + this + " opened.");
            send("HI");
            
            // wait for HELLO command
            while(!isClosed()) {
                cmd = receive();
                if("HELLO".equals(cmd)) {
                    break;
                }
                send("ERROR Client must issue HELLO command.");
            }
    
            send("CHOOSE_USERNAME");
            // wait for USERNAME command
            while(!isClosed()) {
                cmd = receive();
                if(cmd == null || !cmd.startsWith("USERNAME ")) {
                    send("ERROR Client must issue USERNAME command.");
                    continue;
                }
                
                username = commandArg(cmd);
                if(!validUsername(username)) {
                    send("ERROR Username is invalid.");
                } else if(!server.usernameIsAvailable(username)) {
                    send("ERROR Username in use.");
                } else {
                    System.out.println("Connection " + this + " chose username " + username + ".");
                    server.registerUsername(this);
                    break;
                }
            }
            
            send("WELCOME %s", username);
            
            // wait for JOIN or TEST command
            while(!isClosed()) {
                cmd = receive();
                if(cmd.startsWith("JOIN ")) {
                    channelName = commandArg(cmd);
                    if(!validChannelName(channelName)) {
                        send("ERROR Channel name is invalid.");
                        continue;
                    }
                    
                    server.requestMatchup(this, channelName);
                    echoLoop();
                    break;
                } else if(cmd.startsWith("TEST ")) {
                    testProtocol(commandArg(cmd));
                    break;
                } else {
                    send("ERROR Client must issue JOIN or TEST command.");
                }
            }
        } catch(Exception e) {
            if(!isClosed()) {
                System.err.println("Connection " + this + " already closed, caused " + e);
            } else {
                System.err.println("Closing connection " + this + " due to " + e);
                send("ERROR %s", e);
            }
        } finally {
            if(opponent != null && !opponent.isClosed()) {
                opponent.send("OPPONENT_CLOSED %s", username);
            }
            close();
            System.out.println("Connection " + this + " closed.");
        }
    }
    
    private void echoLoop() throws IOException {
        while(!isClosed()) {
            String cmd = receive();
            if(cmd == null || cmd.equals("CLOSE")) {
                close();
                return;
            } else if(opponent != null) {
                if(cmd.startsWith("MOVE ")) {
                    opponent.send("MOVE %s %s", username, commandArg(cmd));
                } else if(cmd.startsWith("CHAT ")) {
                    opponent.send("CHAT %s %s", username, commandArg(cmd));
                } else {
                    send("ERROR Client must issue MOVE or CHAT command.");
                }
            } else {
                send("ERROR Client must wait for READY command.");
            }
        }
    }
    
    private void testProtocol(String gameName) {
        // TODO: implement the TEST command
        send("ERROR The TEST command is not implemented.");
    }
}
