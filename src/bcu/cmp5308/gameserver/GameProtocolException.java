
package bcu.cmp5308.gameserver;

public class GameProtocolException extends Exception {
    public GameProtocolException(String msg) {
        super(msg);
    }
}
